#include <string.h>
#include <stdlib.h>
#include <stdio.h>
int main(int argc, char *argv[])
{
	int len = strlen("Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure");
	printf("len is %d chars\n", len);
	return 0;
}
